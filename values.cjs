// function values(obj) {
//     // Return all of the values of the object's own properties.
//     // Ignore functions
//     // http://underscorejs.org/#values
// }

let values = (obj) => {
    let valuesArr = []
    if(typeof obj !== 'object' || obj === null || Array.isArray(obj)){
        return 'invalid data type'
    }
    for(let key in obj){
        if(obj[key] === undefined){
            continue
        }
        valuesArr.push(obj[key])
    }
    return valuesArr
}

module.exports = values

