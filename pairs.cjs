// function pairs(obj) {
//     // Convert an object into a list of [key, value] pairs.
//     // http://underscorejs.org/#pairs
// }


let pairs = (obj) => {
    if(typeof obj !== 'object' || obj === null || Array.isArray(obj)){
        return 'invalid data type'
    }
    let pairsArr = []
    for(let key in obj){
        pairsArr.push([key, obj[key]])
    }
    return pairsArr
}

module.exports = pairs

