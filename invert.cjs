// function invert(obj) {
//     // Returns a copy of the object where the keys have become the values and the values the keys.
//     // Assume that all of the object's values will be unique and string serializable.
//     // http://underscorejs.org/#invert
// }

let invert = (obj) => {
    if(typeof obj !== 'object' || obj === null || Array.isArray(obj)){
        return 'invalid data type'
    }
    for(let key in obj){
        if(typeof obj[key] === 'function' || typeof obj[key] === 'undefined'){
            return 'invalid datatype'
        }
        if(obj[key] !== undefined){
            obj[obj[key]] = key
            delete obj[key]
        }
    }
    return obj
}

module.exports = {
    invert
}
