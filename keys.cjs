// function keys(obj) {
//     // Retrieve all the names of the object's properties.
//     // Return the keys as strings in an array.
//     // Based on http://underscorejs.org/#keys
// }

let keys = (obj) => {
    let keyArr = []
    for(let key in obj){
        keyArr.push(key)
    }
    return keyArr
}

module.exports = keys
