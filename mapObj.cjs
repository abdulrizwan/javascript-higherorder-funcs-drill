// function mapObject(obj, cb) {
//     // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
//     // http://underscorejs.org/#mapObject
// }

let cb = (value) => {
    value += 5
    return value
}

let mapObject = (obj, cb) => {
    if(typeof obj !== 'object' || obj === null || Array.isArray(obj)){
        return 'invalid data type'
    }
    for(let key in obj){
        if(obj[key] === undefined){
            continue
        }
        obj[key] = cb(obj[key])
    }
    return obj
}

module.exports = {
    mapObject,
    cb
}

