// function defaults(obj, defaultProps) {
//     // Fill in undefined properties that match properties on the `defaultProps` parameter object.
//     // Return `obj`.
//     // http://underscorejs.org/#defaults
// }

let defaults = (obj, defaultProps) => {
    if(typeof obj !== 'object' || obj === null || Array.isArray(obj)){
        return 'invalid data type'
    }
    if(typeof defaultProps !== 'object' || defaultProps === null || Array.isArray(obj)){
        return 'invalid data type'
    }
    for(let key in defaultProps){
        if(obj[key] === undefined && defaultProps[key] !== undefined){
            obj[key] = defaultProps[key]
        }
    }
    return obj
}

module.exports = defaults
